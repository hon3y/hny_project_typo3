/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var wrap = require('gulp-wrap');
var tap = require('gulp-tap');
var util = require('gulp-util');
var debug = require('gulp-debug');
var path = require('path');

var pathToAssets = 'web/typo3conf/ext/**/Resources/Private/Assets/';
var pathToMaskAssets = 'web/typo3conf/ext/**/Resources/Private/Mask/Assets/';

var pathToJs = 'Js/';
var pathToJsLibs = 'includeJSLibs/';
var pathToJsFooterlibs = 'includeJSFooterlibs/';
var pathToJsHeaderData = 'headerData/';
var pathToJsFooterData = 'footerData/';

var pathToScss = 'Scss/';
var pathToCss = 'includeCSS/';
var pathToCustom = 'Custom/';
var pathToImport = 'Import/';
var pathToCssHeaderData = 'headerData/';

var pathPre = 'pre/';
var pathPost = 'post/';

var pathToCompiledFiles = 'web/typo3conf/ext/hny_pot/Resources/Public/Assets/Gulp/';

var paths = {
    src: {
        includeCSS: [
            pathToAssets + pathToScss + pathToCss + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCss + '*.scss',
            pathToMaskAssets + pathToScss + pathToCss + '*.scss',
            pathToAssets + pathToScss + pathToCss + pathPost + '*.scss'
        ],
        includeCssHeaderData: [
            pathToAssets + pathToScss + pathToCss + pathToCssHeaderData + pathPre + '*.scss',
            pathToAssets + pathToScss + pathToCss + pathToCssHeaderData + '*.scss',
            pathToMaskAssets + pathToScss + pathToCss + pathToCssHeaderData + '*.scss',
            pathToAssets + pathToScss + pathToCss + pathToCssHeaderData + pathPost + '*.scss'
        ],
        includeJSLibs: [
            pathToAssets + pathToJs + pathToJsLibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsLibs + '*.js',
            pathToMaskAssets + pathToJs + pathToJsLibs + '*.js',
            pathToAssets + pathToJs + pathToJsLibs + pathPost + '*.js'
        ],
        includeJSFooterlibs: [
            pathToAssets + pathToJs + pathToJsFooterlibs + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsFooterlibs + '*.js',
            pathToMaskAssets + pathToJs + pathToJsFooterlibs + '*.js',
            pathToAssets + pathToJs + pathToJsFooterlibs + pathPost + '*.js'
        ],
        includeJSHeaderData: [
            pathToAssets + pathToJs + pathToJsHeaderData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + '*.js',
            pathToMaskAssets + pathToJs + pathToJsHeaderData + '*.js',
            pathToAssets + pathToJs + pathToJsHeaderData + pathPost + '*.js'
        ],
        includeJSFooterData: [
            pathToAssets + pathToJs + pathToJsFooterData + pathPre + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + '*.js',
            pathToMaskAssets + pathToJs + pathToJsFooterData + '*.js',
            pathToAssets + pathToJs + pathToJsFooterData + pathPost + '*.js'
        ]
    },
    dest: {
        includeCSS: pathToCompiledFiles + pathToScss + pathToCss,
        includeCssHeaderData: pathToCompiledFiles + pathToScss + pathToCssHeaderData,
        includeJSLibs: pathToCompiledFiles + pathToJs + pathToJsLibs,
        includeJSFooterlibs: pathToCompiledFiles + pathToJs + pathToJsFooterlibs,
        includeJSHeaderData: pathToCompiledFiles + pathToJs + pathToJsHeaderData,
        includeJSFooterData: pathToCompiledFiles + pathToJs + pathToJsFooterData
    }
};
var watcherPaths = {
    src: {
        includeCSS: [
            paths.src.includeCSS[0],
            paths.src.includeCSS[1],
            pathToAssets + pathToScss + pathToCustom + '**/*.scss',
            pathToAssets + pathToScss + pathToImport + '**/*.scss',
            paths.src.includeCSS[2]
        ]
    }
};

/**
 * Scss => Css
 */
gulp.task('includeCSS', function() {
    gulp.src(
        paths.src.includeCSS
    )
    .pipe(concat('includeCSS.css'))
    .pipe(sass({
        paths: [ 'web' ]
    }).on('error', sass.logError))
    .pipe(rename('includeCSS.min.css'))
    .pipe(autoprefixer({
        browsers: ['last 3 versions', 'safari 7', 'safari 8']
    }))
    .pipe(cleanCSS(
        {
            level: {
                2: {
                    // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                    // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                    mergeMedia: false, // controls `@media` merging; defaults to true
                    mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                    // mergeSemantically: true // controls semantic merging; defaults to false
                    // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                    // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                    // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                    // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                    // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                    // restructureRules: true // controls rule restructuring; defaults to false
                }

            }
        }
    ))
    .pipe(gulp.dest(paths.dest.includeCSS))
});
gulp.task('includeCssHeaderData', function() {
    return gulp.src(
        paths.src.includeCssHeaderData
    )
    .pipe(sass())
    .pipe(rename('includeCssHeaderData.min.css'))
    .pipe(autoprefixer({
        browsers: ['last 3 versions', 'safari 7', 'safari 8']
    }))
    .pipe(cleanCSS(
        {
            level: {
                2: {
                    mergeMedia: false, // controls `@media` merging; defaults to true
                    mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                }
            }
        }
    ))
    .pipe(wrap('page{\nheaderData.333333 = TEXT\nheaderData.333333.value ( \n<%= contents %>\n)\nheaderData.333333.wrap =<style>|</style>\n}\n\n'))
    .pipe(gulp.dest(paths.dest.includeCssHeaderData));
});

/**
 * Js => Js.min
 */
gulp.task('includeJSLibs', function() {
    return gulp.src(
        paths.src.includeJSLibs
        )
        .pipe(concat('includeJSLibs.js'))
        .pipe(gulp.dest(paths.dest.includeJSLibs))
        .pipe(rename('includeJSLibs.min.js'))
        .pipe(uglify({compress: {
            comparisons: false,
            conditionals: false
        }}))
        .pipe(gulp.dest(paths.dest.includeJSLibs));
});
gulp.task('includeJSFooterlibs', function() {
    return gulp.src(
        paths.src.includeJSFooterlibs
        )
        .pipe(concat('includeJSFooterlibs.js'))
        .pipe(gulp.dest(paths.dest.includeJSFooterlibs))
        .pipe(rename('includeJSFooterlibs.min.js'))
        .pipe(uglify({compress: {
            comparisons: false,
            conditionals: false
        }}))
        .pipe(gulp.dest(paths.dest.includeJSFooterlibs));
});
gulp.task('includeJSHeaderData', function() {
    return gulp.src(
        paths.src.includeJSHeaderData
        )
        .pipe(concat('headerData.js'))
        .pipe(gulp.dest(paths.dest.includeJSHeaderData))
        .pipe(rename('headerData.ts'))
        .pipe(uglify({compress: {
            comparisons: false,
            conditionals: false
        }}))
        .pipe(wrap('page {\nheaderData.6666666 = TEXT\nheaderData.6666666.value ( \n<%= contents %>\n)\nheaderData.6666666.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.includeJSHeaderData));
});

gulp.task('includeJSFooterData', function() {
    return gulp.src(
        paths.src.includeJSFooterData
        )
        .pipe(concat('footerData.js'))
        .pipe(gulp.dest(paths.dest.includeJSFooterData))
        .pipe(rename('footerData.ts'))
        .pipe(uglify({compress: {
            comparisons: false,
            conditionals: false
        }}))
        .pipe(wrap('page {\nfooterData.9999999 = TEXT\nfooterData.9999999.value ( \n<%= contents %>\n)\nfooterData.9999999.wrap =<script>|</script>\n}\n\n'))
        .pipe(gulp.dest(paths.dest.includeJSFooterData));
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'includeCSS',
        'includeCssHeaderData',
        'includeJSLibs',
        'includeJSFooterlibs',
        'includeJSHeaderData',
        'includeJSFooterData'
    ],
    function() {
        gulp.watch(watcherPaths.src.includeCSS, ['includeCSS'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(paths.src.includeCssHeaderData, ['includeCssHeaderData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(paths.src.includeJSLibs, ['includeJSLibs'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(paths.src.includeJSFooterlibs, ['includeJSFooterlibs'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(paths.src.includeJSHeaderData, ['includeJSHeaderData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(paths.src.includeJSFooterData, ['includeJSFooterData'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
    }
);