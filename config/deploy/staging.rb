server '<hon3y.firma.cc>', user: 'ssh-<user>', roles: %w{web}, ssh_options: { forward_agent: true }

##
## :hon3y_staging_date
##
## e.g. set :hon3y_staging_date, "<yyyymmddhhmm>"
##
set :hon3y_staging_date, "<yyyymmddhhmm>"

##
## :hon3y_db_ini
##
## e.g. set :hon3y_db_ini, "staging_1.ini"
##
set :hon3y_db_ini, "<staging_1.ini>"

##
## :hon3y_customer_abbr
##
## Customer Abbr.
## e.g. set :deploy_to, "teu"
##
set :hon3y_customer_abbr, "<customer_abbr>"

##
## :hon3y_type
##
## e.g. set :hon3y_type, "ws"
##
set :hon3y_type, "ws"

##
## :hon3y_year
##
## e.g. set :hon3y_year, "17"
##
set :hon3y_year, "17"

##
## :hon3y_stage
##
## e.g. set :hon3y_stage, "staging"
##
set :hon3y_stage, "staging"

##
## :hon3y_custom_extension
##
## e.g. set :hon3y_custom_extension, "hny_pot"
## e.g. set :hon3y_custom_extension, "hive_thm_custom"
##
set :hon3y_custom_extension, "hny_pot"

##
## :hon3y_node_modules_bin
##
## Absolute path to node_modules
## e.g. set :hon3y_node_modules_bin, "./node_modules/.bin"
##
set :hon3y_node_modules_bin, "./node_modules/.bin"

##
## :hon3y_php_cli_bin
##
## Absolute path to PHP CLI
##
## DomainFactory:
## e.g. set :hon3y_php_cli_bin, "/usr/local/bin/php7-70STABLE-CLI"
##
## Mittwald:
## e.g. set :hon3y_php_cli_bin, "/usr/local/bin/php_cli"
##
set :hon3y_php_cli_bin, "/usr/local/bin/php7-70STABLE-CLI"

##
## :hon3y_php_bin
##
## Absolute path to PHP
##
## DomainFactory:
## e.g. set :hon3y_php_bin, "/usr/local/bin/php7-70STABLE-STANDARD"
## e.g. set :hon3y_php_bin, "/usr/local/bin/php7-70STABLE-FCGI"
##
## Mittwald:
## e.g. set :hon3y_php_cli_bin, "/usr/local/bin/php"
##
set :hon3y_php_bin, "/usr/local/bin/php7-70STABLE-STANDARD"

##
## :hon3y_server_path
##
## Absolute server path
##
## DomainFactory:
## e.g. set :deploy_to, "/www/470890_78628/webseiten"
##
## Mittwald:
## e.g. set :deploy_to, "/home/www/p123456/html/DEV"
##
set :hon3y_server_path, "/www/470890_78628/webseiten"

##
## :hon3y_quota
##
## Absolute path to quota
## e.g. set :hon3y_quota, "#{fetch(:hon3y_server_path)}/#{fetch(:hon3y_customer_abbr)}#{fetch(:hon3y_type)}"
##
set :hon3y_quota, "#{fetch(:hon3y_server_path)}/#{fetch(:hon3y_customer_abbr)}/#{fetch(:hon3y_type)}"

##
## :hon3y_fileadmin
## :hon3y_uploads
## 
## Absolute paths to shared folders (e.g. fileadmin, uploads,...) 
##
## set :hon3y_fileadmin, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/fileadmin/#{fetch(:hon3y_staging_date)}"
## set :hon3y_uploads, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/uploads/#{fetch(:hon3y_staging_date)}"
##
## IMPORTANT !!!
## Add shared folders in production.rb and ../deploy.rb
##
set :hon3y_gulp, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/gulp/#{fetch(:hon3y_staging_date)}"
set :hon3y_fileadmin, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/fileadmin/#{fetch(:hon3y_staging_date)}" 
set :hon3y_uploads, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/uploads/#{fetch(:hon3y_staging_date)}"

################################################################################################################################
################################################################################################################################
##
## Do not edit anything below or clowns will eat you
##
################################################################################################################################
################################################################################################################################

##
## :deploy_to
##
## Absolute path to quota + year and staging folder
## e.g. set :deploy_to, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/staging"
##
set :deploy_to, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}"

##
## :hon3y_config
## :hon3y_database_ini
## :hon3y_sudo_ini
## :hon3y_database_dump_definition
## :hon3y_database_dump_data
##
## Absolute path to shared config / ini / dump
## e.g. set :hon3y_config, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config"
## e.g. set :hon3y_database_ini, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config/#{fetch(:hon3y_db_ini)}"
## e.g. set :hon3y_sudo_ini, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config/sudo.ini"
## e.g.set :hon3y_database_dump_definition, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/dump/#{fetch(:hon3y_staging_date)}/definition.sql"
## e.g.set :hon3y_database_dump_data, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/dump/#{fetch(:hon3y_staging_date)}/data.sql"
##
set :hon3y_config, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config" 
set :hon3y_database_ini, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config/#{fetch(:hon3y_db_ini)}"
set :hon3y_sudo_ini, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/config/sudo.ini"
set :hon3y_database_dump_definition, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/dump/#{fetch(:hon3y_staging_date)}/definition.sql"
set :hon3y_database_dump_data, "#{fetch(:hon3y_quota)}/#{fetch(:hon3y_year)}/#{fetch(:hon3y_stage)}/shared/dump/#{fetch(:hon3y_staging_date)}/data.sql"

namespace :staging do

  task :hon3y_staging do
    on roles(:web) do
      # nothing right now
    end
  end

  after 'deploy:finishing', 'staging:hon3y_staging'
end