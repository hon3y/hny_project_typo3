#!/bin/bash

export $(cat .env | grep -v ^# | xargs)

echo ""
echo "HIVE | installer\n"

echo "php vendor/bin/typo3cms install:setup ..."

php vendor/bin/typo3cms install:setup \
  --non-interactive \
  --use-existing-database \
  --database-user-name="dev" \
  --database-host-name="mysql" \
  --database-port="3306" \
  --database-name="dev" \
  --admin-user-name="sudo" \
  --admin-password="${ADMIN_PASSWORD}" \
  --site-name="Basic Install"

echo "touch ./web/typo3conf/ENABLE_INSTALL_TOOL"

touch ./web/typo3conf/ENABLE_INSTALL_TOOL

echo ""
echo "User: sudo"
echo "Password: ${ADMIN_PASSWORD}"
echo ""
echo "Please add custom users via install tool."

echo ""
echo "Done :)"