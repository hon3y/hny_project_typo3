![version 8.11.0](https://img.shields.io/badge/version-8.11.0-green.svg?style=flat-square)
   ![license MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

```
NAME
    hny_project_typo3

SYNOPSIS
    Good starting point for a TYPO3 project

DESCRIPTION
    Install TYPO3 in a preconfigured docker machine. Deploy your project via bitbucket piplines.

REQUIREMENTS
    Git
    Docker (for Mac) [https://www.docker.com/]
    Docker Compose [https://docs.docker.com/compose/]
    Docker Sync (on Mac) [http://docker-sync.io/]
    Bash  

INSTALLATION 

    On Bitbucket:

        Create a (private) project and fork 'hny_project_typo3' and 'hive_thm_custom' into the project.

        Name the repos 'hny_project_typo3' and '<customer_abbr>_<yy>_<type>_thm'

        Create a branch 'custom' in '<customer_abbr>_<yy>_<type>_thm' from most recent 'release/X.Y' branch.
    
    On your computer:

        Creat a project with your preferred IDE.

        Clone the fork of 'hny_project_typo3' into your project.

        Edit '.env' and replace placeholders with a unique value. Otherwise docker would override containers from multiple projects with each other.

        Edit 'composer.json' and replace placeholder with '<customer_abbr>_<yy>_<type>_thm'.

        Start docker (e.g. via 'sudo systemctl start docker.service' on arch linux)

        Run 'sudo make up-on-linux' or 'make up-on-osx' to build the docker containers.

        Change into the built app container via 'sudo make root-on-linux' or make 'root-on-osx'.

            Create a '.ssh' folder in '/root/'

            Copy your private and public key (which is configured in your bitbucket account) to '/root/.ssh/'

            Creat a config in '/root/.ssh/' which contains at least:

                Host bitbucket.org
                    IdentityFile ~/.ssh/your_private_key

            Run 'chmod -R 600 /root/.ssh/'

            Run 'sh /app/docker_install_typo3.sh'.

            Run 'mysql -hmysql dev -udev -pdev < /app/171009_dummy.sql'

            Optional run '-hmysql dev -udev -pdev < /app/171009_guest.sql'

            Optional run 'chmod -R 777 /app/web/'

        Change into built phpgulp container via 'sudo make gulp-on-linux' or 'make gulp-on-osx'.

            Run 'gulp --gulpfile=docker_gulpfile.js'.


        ENJOY a ready to use TYPO3 installation :)


LISENCE:
    The MIT License (MIT)

    Copyright (c) 2018

    Dominik Hilser <d.hilser@hon3y.rocks>,
    Georg Kathan <g.kathan@hon3y.rocks>,
    Perrin Ennen <p.ennen@hon3y.rocks>,

    Andreas Hafner <a.hafner@teufels.com>,
    Hendrik Krüger <h.krueger@teufels.com>,
    Josymar Escalona Rodriguez <j.rodriguez@teufels.com>,
    Timo Bittner <t.bittner@teufels.com>,
    Yannick Aister <y.aister@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
```