#!/bin/bash

echo ""
echo "HIVE | Generate PackageStates \n"

echo "cd /app/web/typo3conf/ext/\n"

cd /app/web/typo3conf/ext/

echo "ln -s ../../../vendor/helhum/typo3-console typo3_console\n"

ln -s ../../../vendor/helhum/typo3-console typo3_console

echo "cd /app/\n"

cd /app/

php vendor/bin/typo3cms install:generatepackagestates \
    --activate-default