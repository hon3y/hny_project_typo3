<?php

if (extension_loaded('Zend OPcache') && ini_get('opcache.enable') === '1') {
    if (function_exists('opcache_reset')) {
        opcache_reset();
    }
}

?>