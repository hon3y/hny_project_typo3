#!/bin/bash

ln -s ../../../vendor/helhum/typo3-console ./web/typo3conf/ext/typo3_console

ls -all

cp -Lp web/typo3conf/server.ini temp_current.ini

ls -all

. $(dirname "$0")/temp_current.ini

charset="$charset"
dbname="$dbname"
host="$host"
password="$password"
port="$port"
user="$user"

cp -Lp sudo.ini temp_sudo.ini

ls -all

. $(dirname "$0")/temp_sudo.ini

##
## Backup database
##    
TIME=$(date +"%Y%m%d%H%M%S")

touch ./temp_defaults-extra-file.cnf
echo "[client]" >> ./temp_defaults-extra-file.cnf
echo "  user=${user}" >> ./temp_defaults-extra-file.cnf
echo "  password='${password}'" >> ./temp_defaults-extra-file.cnf
echo "  host=${host}" >> ./temp_defaults-extra-file.cnf
echo "  database=${dbname}" >> ./temp_defaults-extra-file.cnf

mysqldump --defaults-extra-file=./temp_defaults-extra-file.cnf --single-transaction --skip-add-locks --no-autocommit > ./${TIME}_${dbname}_dump.sql

admin="${admin}"
admin_password="${admin_password}"

$1 -c ./php.ini ./web/typo3conf/ext/typo3_console/Scripts/typo3cms install:setup \
    --non-interactive \
    --use-existing-database \
    --database-user-name="${user}" \
    --database-host-name="${host}" \
    --database-port="${port}" \
    --database-name="${dbname}" \
    --admin-user-name="${admin}" \
    --admin-password="${admin_password}" \
    --site-name="Installation" 
    
rm -f temp_current.ini
rm -f temp_sudo.ini

ls -all